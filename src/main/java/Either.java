import java.util.Objects;
import java.util.function.Consumer;

public final class Either<L, R> {

    private final L left;
    private final R right;

    private Either() {
        left = null;
        right = null;
    }

    private Either(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public static <L, R> Either<L, R> left(L left) {
        Objects.requireNonNull(left);
        return new Either<L, R>(left, null);
    }

    public static <L, R> Either<L, R> right(R right) {
        Objects.requireNonNull(right);
        return new Either<L, R>(null, right);
    }

    public boolean isLeft() {
        return left != null;
    }

    public boolean isRight() {
        return right != null;
    }

    public L getLeft() {
        return this.left;
    }

    public R getRight() {
        return this.right;
    }

    public void consume(Consumer<L> leftConsumer, Consumer<R> rightConsumer) {
        if (isLeft()) {
            leftConsumer.accept(left);
        }
        if (isRight()) {
            rightConsumer.accept(right);
        }
    }
}
