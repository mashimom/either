# Implementation of functional either concept

[![pipeline status](https://gitlab.com/mashimom/either/badges/master/pipeline.svg)](https://gitlab.com/mashimom/either/commits/master)
[![unit test coverage report](https://gitlab.com/mashimom/either/badges/master/coverage.svg?job=test)](https://gitlab.com/mashimom/either/commits/master)
[![integration test coverage report](https://gitlab.com/mashimom/either/badges/master/coverage.svg?job=integrationTest)](https://gitlab.com/mashimom/either/commits/master)


> Consider that you can have two types that cannot semantically overlap:
> * movement vs. inertia
> * ...


## Goals

Have a good implemtation of the functional programming _Either_ concept for Java, a natural evolution of using Optional. Dealing with two possible outcomes of a compute.

## Features

* [ ] Functional Interface Either
* [ ] Sample usage
